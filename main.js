"user strict";

$(document).ready(function() {
//Initialize the Backbone model
  var Categorize = Backbone.Model.extend({
        url: "http://dunatis.library.illinois.edu:9200/categorize/category/",
        asset_id: '',
        company: '',
        category: '',
  });

//Initialize backbone collection  
var NewImage = Backbone.Collection.extend({
  url: 'http://dunatis.library.illinois.edu:9200/categorize/asset/_search',
}); 
  
  //build the objects, fetch the data, render on success
  newimage = new NewImage();
  var table = $( "#table" );
  var counter=0; // this is the counter determines which image to retrieve
  var variables; //image collection instance
  data_error = "Failed to load data, please refresh the page.";

  newimage.fetch({
    dataType: "json",
    success: function() {
        imageList.render();
    },
    error: function() {
          image_container.html( data_error );
    },
  });

//Initialize the Viewing method
var ImageList = Backbone.View.extend({
  el: table,
  collection: newimage,

  events: {
    "click #submit": "categorize",
    "click #switch": "switch",
  },  

  render: function(event){
    //iterate through all images to categorize        
    newimage.each(function(model){
      variables = {
        id: model.attributes.hits.hits[counter]._id,
        _index: model.attributes.hits.hits[counter]._index,
        _score: model.attributes.hits.hits[counter]._score,
        Name: model.attributes.hits.hits[counter]._source.Name,
        picurl: model.attributes.hits.hits[counter]._source.Url,
        _type: model.attributes.hits.hits[counter]._type,
      };
      debugger;
      if (variables.picurl != "") {
        var template = _.template( $("#crowdsourcetemplate").html(), variables);
        table.append( template); 
       
      }
    });
    console.log("*images rendered succesfully*");
    debugger;
    $(".img-img").attr("src", variables.picurl);
    //$(".fancy-box").attr("href", variables.picurl);

    var wid =$(".img-img").attr('width');
    wid = wid*2;
    var hei =$(".img-img").attr('height');
    hei = hei*2; 
    // $(".img-img").attr("width", wid);
    // $(".img-img").attr("height", hei);

    debugger
    jQuery(".fancy-box").attr('rel', 'gallery').fancybox({
      helpers : {
        overlay : {
            css : {
                'height': hei,
                'width': wid,
            }
        }
      },

    margin: 0,
    padding: 0,
    autoSize : false,
    fitToView : false,
    height: '1000px',
    width: '1000px',


   
      afterLoad  : function () {
            if (this.title) {
                // New line
                this.title += '<br />';
                
                // Add tweet button
                this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-text="Join Crowd-source categorization with us!" data-size="large">Tweet</a>';
                
                // Add FaceBook like button
                this.title += '<iframe src="https://www.facebook.com/plugins/like.php?href=' + this.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:23px;" allowTransparency="true"></iframe>';
            }

            $.extend(this, {
                aspectRatio : false,
                type    : 'html',
                content : '<div class="fancybox-image" style="display: inline-block;background-image:url(' + variables.picurl + '); background-position: center; background-size:contain; background-repeat:no-repeat;" /></div>'
            });
             $('.fancy-box').resize();
        debugger;
            debugger;
      },

      beforeShow: function() {
    $('.img-img')
        .width($(window).width())
        .height($(window).height());
},
onUpdate: function() {
    $('.img-img')
        .width($(window).width())
        .height($(window).height());
},

      onComplete : function(){
        $('.fancy-box').resize();
        debugger;
    },
    afterShow: function() {
            // Render tweet button
            twitter.widgets.load();
        },

      afterClose : function() {
        // switch image automatically when categorization is done, and user wants to start on next image
          var recentimage = new NewImage ();
          recentimage.fetch({
            dataType: "json",
            success: function() {
              recentimage.each(function(model){
                variables = {
                  id: model.attributes.hits.hits[counter]._id,
                  _index: model.attributes.hits.hits[counter]._index,
                  _score: model.attributes.hits.hits[counter]._score,
                  Name: model.attributes.hits.hits[counter]._source.Name,
                  picurl: model.attributes.hits.hits[counter]._source.Url,
                  _type: model.attributes.hits.hits[counter]._type,
                };
                if (variables.picurl != "") {
                  table.html("");
                  var template = _.template( $("#crowdsourcetemplate").html(), variables);
                  table.append( template );
                }
              });
            },
            error: function() {
              image_container.html( data_error );
            },
          });

        return;}


    });
    //$(document).ready(function() {

      //$(".fancy-box").update();
      //$("tag").fancybox.scrollBox();
     
    debugger;
  },

  // send categorization information in JSON to the server
  categorize: function(event) {
    var data =$('form').serialize();  // this will produce "select=Beauty&company=Google" for example
    console.log(data);
    var arr1=data.split('&');
    var arr2=arr1[0].split('=');
    var arr3=arr1[1].split('=');
    //var categorization = new Categorize({asset_id:variables.id, company:arr3[1], category:arr2[1]});
    if (arr3[1]==""){
      alert("Put in the company name for categorization! Thanks");
      return;
    }
    $.ajax({
      url: 'http://dunatis.library.illinois.edu:9200/categorize/category/',
      type: 'POST',
      dataType: 'json',
      data: JSON.stringify({
        "asset_id": variables.id,
        "company" : arr3[1],
        "category": arr2[1],
      }),
      success: function(){
      //   $(".btn btn-default").confirm({
      //     text: "Categorization sent successfully, Thank you! \n Start categorization of another image?",
      //     title: "New Categorization",
      //     confirm: function(button) {
      //         // switch image automatically when categorization is done, and user wants to start on next image
      //       var recentimage = new NewImage ();
      //       recentimage.fetch({
      //         dataType: "json",
      //         success: function() {
      //           counter = (counter+1)%10;
      //           recentimage.each(function(model){
      //             var variables = {
      //               id: model.attributes.hits.hits[counter]._id,
      //               _index: model.attributes.hits.hits[counter]._index,
      //               _score: model.attributes.hits.hits[counter]._score,
      //               Name: model.attributes.hits.hits[counter]._source.Name,
      //               picurl: model.attributes.hits.hits[counter]._source.Url,
      //               _type: model.attributes.hits.hits[counter]._type,
      //             };
      //             if (variables.picurl != "") {
      //               table.html("");
      //               var template = _.template( $("#crowdsourcetemplate").html(), variables);
      //               table.append( template );
      //             }
      //           });




      //         },
      //         error: function() {
      //           image_container.html( data_error );
      //         },
      //       });
      //     },
      //     cancel: function(button) {
      //         // nothing to do
      //     },
      //     confirmButton: "Yes",
      //     cancelButton: "Cancel and stay on current page",
      //     post: true,
      //     confirmButtonClass: "btn-danger",
      //     cancelButtonClass: "btn-default",
      //     dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
      // });






        var r = confirm("Categorization sent successfully, Thank you! \n Start categorization of another image?");
        if (r == true) {
          // switch image automatically when categorization is done, and user wants to start on next image
          var recentimage = new NewImage ();
          recentimage.fetch({
            dataType: "json",
            success: function() {
              counter = (counter+1)%10;
              recentimage.each(function(model){
                variables = {
                  id: model.attributes.hits.hits[counter]._id,
                  _index: model.attributes.hits.hits[counter]._index,
                  _score: model.attributes.hits.hits[counter]._score,
                  Name: model.attributes.hits.hits[counter]._source.Name,
                  picurl: model.attributes.hits.hits[counter]._source.Url,
                  _type: model.attributes.hits.hits[counter]._type,
                };
                if (variables.picurl != "") {
                  table.html("");
                  var template = _.template( $("#crowdsourcetemplate").html(), variables);
                  table.append( template );
                 (function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}})(document, 'script', 'twitter-wjs');
                }
              });




            },
            error: function() {
              image_container.html( data_error );
            },
          });
        } else {
            
        }
      },
      error: function(e) {
        console.log(e);
      }
    });
  },

//switch image button event
  switch: function(event) {
    var recentimage = new NewImage ();
    recentimage.fetch({
      dataType: "json",
      success: function() {
        counter = (counter+1)%10;
        recentimage.each(function(model){
           variables = {
            id: model.attributes.hits.hits[counter]._id,
            _index: model.attributes.hits.hits[counter]._index,
            _score: model.attributes.hits.hits[counter]._score,
            Name: model.attributes.hits.hits[counter]._source.Name,
            picurl: model.attributes.hits.hits[counter]._source.Url,
            _type: model.attributes.hits.hits[counter]._type,
          };
          if (variables.picurl != "") {
            table.html("");
            var template = _.template( $("#crowdsourcetemplate").html(), variables);
            table.append( template );
          }
        });
        //!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
          (function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}})(document, 'script', 'twitter-wjs');



               jQuery(".fancy-box").attr('rel', 'gallery').fancybox({
      helpers : {
        overlay : {
            css : {
                'height': hei,
                'width': wid,
            }
        }
      },

    margin: 0,
    padding: 0,
    autoSize : false,
    fitToView : false,
    height: '1000px',
    width: '1000px',


   
      afterLoad  : function () {
            if (this.title) {
                // New line
                this.title += '<br />';
                
                // Add tweet button
                this.title += '<a href="https://twitter.com/share" class="twitter-share-button" data-text="Join Crowd-source categorization with us!" data-size="large">Tweet</a>';
                
                // Add FaceBook like button
                this.title += '<iframe src="https://www.facebook.com/plugins/like.php?href=' + this.href + '&amp;layout=button_count&amp;show_faces=true&amp;width=500&amp;action=like&amp;font&amp;colorscheme=light&amp;height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:110px; height:23px;" allowTransparency="true"></iframe>';
            }

            $.extend(this, {
                aspectRatio : false,
                type    : 'html',
                content : '<div class="fancybox-image" style="display: inline-block;background-image:url(' + variables.picurl + '); background-position: center; background-size:contain; background-repeat:no-repeat;" /></div>'
            });
             $('.fancy-box').resize();
        debugger;
            debugger;
      },

      beforeShow: function() {
    $('.img-img')
        .width($(window).width())
        .height($(window).height());
},
onUpdate: function() {
    $('.img-img')
        .width($(window).width())
        .height($(window).height());
},

      onComplete : function(){
        $('.fancy-box').resize();
        debugger;
    },
    afterShow: function() {
            // Render tweet button
            twitter.widgets.load();
        },

      afterClose : function() {
        // switch image automatically when categorization is done, and user wants to start on next image
          var recentimage = new NewImage ();
          recentimage.fetch({
            dataType: "json",
            success: function() {
              recentimage.each(function(model){
                variables = {
                  id: model.attributes.hits.hits[counter]._id,
                  _index: model.attributes.hits.hits[counter]._index,
                  _score: model.attributes.hits.hits[counter]._score,
                  Name: model.attributes.hits.hits[counter]._source.Name,
                  picurl: model.attributes.hits.hits[counter]._source.Url,
                  _type: model.attributes.hits.hits[counter]._type,
                };
                if (variables.picurl != "") {
                  table.html("");
                  var template = _.template( $("#crowdsourcetemplate").html(), variables);
                  table.append( template );
                }
              });
            },
            error: function() {
              image_container.html( data_error );
            },
          });

        return;}


    });













      },
      error: function() {
        image_container.html( data_error );
      },
    });
  },

  zoom: function(){
    //$("a#fancy-box").fancybox();
  }

});
//construct the object so we can render it later
  var imageList = new ImageList();

});